<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day6 extends Day {

  protected const DAY = 6;

  public function __construct() {
    $this->addExample(1, 1, "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb", "11");
    $this->addExample(2, 1, "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb", "6");
  }

  public function processInputs(array $inputs): array {
    $answers = [];
    $answer = [];
    $count = 0;
    foreach ($inputs as $key => $input) {
      if ($input !== '') {
        $input = str_split($input);
        $input = array_flip($input);

        $answers[$count][] = $input;
      }
      else {
        ksort( $answers[$count]);
        $answer = [];
        $count++;
      }
    }
    if ($answer) {
      ksort($answer);
      $answers[] = $answer;
    }

    return $answers;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $sum = 0;
    foreach ($inputs as $input) {
      $qAnswers = $input[0];
      foreach ($input as $qAnswer) {
        $qAnswers = array_merge($qAnswers, $qAnswer);
        ksort($qAnswers);
      }
      $sum += count($qAnswers);
    }

    $answer = $sum;
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $sum = 0;
    foreach ($inputs as $input) {
      if (count($input) > 1) {
        $qAnswers = array_intersect_key(...$input);
      }
      else {
        $qAnswers = $input[0];
      }
      ksort($qAnswers);
      $sum += count($qAnswers);
    }

    $answer = $sum;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
