<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day18 extends Day {

  protected const DAY = 18;

  public function __construct() {
    $this->addExample(1, 0, "1 + 2 * 3 + 4 * 5 + 6", "71");
    $this->addExample(1, 1, "1 + (2 * 3) + (4 * (5 + 6))", "51");
    $this->addExample(1, 2, "2 * 3 + (4 * 5)", "26");
    $this->addExample(1, 3, "5 + (8 * 3 + 9 + 3 * 4 * 3)", "437");
    $this->addExample(1, 4, "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", "12240");
    $this->addExample(1, 5, "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", "13632");
    $this->addExample(2, 1, "1 + 2 * 3 + 4 * 5 + 6", "231");
    $this->addExample(2, 1, "1 + (2 * 3) + (4 * (5 + 6))", "51");
    $this->addExample(2, 2, "2 * 3 + (4 * 5)", "46");
    $this->addExample(2, 3, "5 + (8 * 3 + 9 + 3 * 4 * 3)", "1445");
    $this->addExample(2, 4, "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", "669060");
    $this->addExample(2, 5, "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", "23340");
  }

  public function processInputs(array $inputs): array {
    foreach ($inputs as &$input) {
      $input = str_replace('(', '( ', $input);
      $input = str_replace(')', ' )', $input);
    }
    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $precedence = [
      ['('],
      ['+', '*'],
    ];

    $answer = 0;
    foreach ($inputs as $equation) {
      $answer += $this->evaluateStatement($equation, $precedence);
    }

    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }


  public function evaluateStatement(string $statement, $precedence) {
    $statement = explode(' ', $statement);

    $answer = FALSE;
    $left = FALSE;
    $right = FALSE;
    $op = FALSE;
    for ($i = 0; $i < count($statement); $i++) {
      if ($answer !== FALSE) {
        $left = $answer;
      }

      $pos = $statement[$i];

      if ($statement[$i] == '(') {
        $subStatement = '';
        $open = 0;
        for($j = $i+1; $j < $statement; $j++) {
          if ($statement[$j] == '(') {
            $open++;
            $subStatement .= ' ' . $statement[$j];
          }
          elseif($statement[$j] == ')') {
            if ($open == 0) {
              $pos = $this->evaluateStatement($subStatement, $precedence);
              $s = str_replace("($subStatement )", $pos, implode(' ', $statement));
              $statement = explode(' ', $s);
              echo implode(' ', $statement) . "\n";
              break;
            }
            else {
              $subStatement .= ' ' . $statement[$j];
              $open--;
            }
          }
          else {
            $subStatement .= ' ' . $statement[$j];
          }
        }
      }

      if ($pos === ' ' || $pos === '') {
        continue;
      }

      if ($left === FALSE) {
        $left = $pos;
        continue;
      }

      if ($left !== FALSE && $op === FALSE) {
        $op = $pos;
        continue;
      }
      else {
        $right = $pos;
      }

      if ($left !== FALSE && $right !== FALSE & $op !== FALSE) {
        switch ($op) {
          case '*':
            $answer = $left * $right;
          break;
          case '+':
            $answer = $left + $right;
          break;
        }

        $left = FALSE;
        $right = FALSE;
        $op = FALSE;
      }
    }

    return $answer;
  }

  public function evaluateStatement2(string $statement) {
    do {
      $statement = $this->processAdds($statement);
      $statement = implode(' ', $statement);
      echo "$statement\n";
    } while(strpos($statement, '+') !== FALSE);

    $statement = explode(' ', $statement);

    $answer = FALSE;
    $left = FALSE;
    $right = FALSE;
    $op = FALSE;
    for ($i = 0; $i < count($statement); $i++) {
      if ($answer !== FALSE) {
        $left = $answer;
      }

      $pos = $statement[$i];

      if ($statement[$i] == '(') {
        $subStatement = '';
        $open = 0;
        for($j = $i+1; $j < $statement; $j++) {
          if ($statement[$j] == '(') {
            $open++;
            $subStatement .= ' ' . $statement[$j];
          }
          elseif($statement[$j] == ')') {
            if ($open == 0) {
              $pos = $this->evaluateStatement2($subStatement);
              $s = str_replace("($subStatement )", $pos, implode(' ', $statement));
              $statement = explode(' ', $s);
              break;
            }
            else {
              $subStatement .= ' ' . $statement[$j];
              $open--;
            }
          }
          else {
            $subStatement .= ' ' . $statement[$j];
          }
        }
      }

      if ($pos === ' ' || $pos === '') {
        continue;
      }

      if ($left === FALSE) {
        $left = $pos;
        continue;
      }

      if ($left !== FALSE && $op === FALSE) {
        $op = $pos;
        continue;
      }
      else {
        $right = $pos;
      }

      if ($left !== FALSE && $right !== FALSE & $op !== FALSE) {
        switch ($op) {
          case '*':
            $answer = $left * $right;
          break;
          case '+':
            $answer = $left + $right;
          break;
        }

        $left = FALSE;
        $right = FALSE;
        $op = FALSE;
      }
    }

    return $answer === FALSE ? (int) implode($statement) : $answer;
  }

  public function processAdds($statement) {
    $statement = explode(' ', $statement);

    $answer = FALSE;
    $left = FALSE;
    $right = FALSE;
    $op = FALSE;
    for ($i = 0; $i < count($statement); $i++) {
      if ($answer !== FALSE) {
        $left = $answer;
      }

      $pos = $statement[$i];

      if ($statement[$i] == '(') {
        $subStatement = '';
        $open = 0;
        for($j = $i+1; $j < $statement; $j++) {
          if ($statement[$j] == '(') {
            $open++;
            $subStatement .= ' ' . $statement[$j];
          }
          elseif($statement[$j] == ')') {
            if ($open == 0) {
              $pos = $this->evaluateStatement2($subStatement);
              $s = str_replace("($subStatement )", $pos, implode(' ', $statement));
              $statement = explode(' ', $s);
              echo implode(' ', $statement) . "\n";
              break;
            }
            else {
              $subStatement .= ' ' . $statement[$j];
              $open--;
            }
          }
          else {
            $subStatement .= ' ' . $statement[$j];
          }
        }
      }

      if ($pos === ' ' || $pos === '') {
        continue;
      }

      if ($left === FALSE) {
        $left = $pos;
        continue;
      }

      if ($left !== FALSE && $op === FALSE) {
        $op = $pos;
        continue;
      }
      else {
        $right = $pos;
      }

      if ($left !== FALSE && $right !== FALSE & $op !== FALSE) {
        switch ($op) {
          case '+':
            $s = str_replace("$left + $right", $left + $right, implode(' ', $statement));
            $statement = explode(' ', $s);
            $i -= 2;
            $left = $left + $right;
            $right = FALSE;
            $op = FALSE;
            break;
          default:
            $left = $right;
            $right = FALSE;
            $op = FALSE;
            break;
        }


      }
    }

    return $statement;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $answer = 0;
    foreach ($inputs as $equation) {
      echo "\n$equation\n";
      $answer += $this->evaluateStatement2($equation);
    }

    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
