<?php

namespace tests2020;

use y2020\Day22 as Day;
use y2020\src\DayInterface;

final class Day22Test extends DayTestBase {

  protected function setUp(): void {
    $this->day = new Day();
  }

  protected static function getDay(): DayInterface {
    return new Day();
  }

}