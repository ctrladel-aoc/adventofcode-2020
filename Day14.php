<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day14 extends Day {

  protected const DAY = 14;

  public function __construct() {
    $this->addExample(1, 1, "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\nmem[8] = 11\nmem[7] = 101\nmem[8] = 0", "165");
    $this->addExample(2, 1, "mask = 000000000000000000000000000000X1001X\nmem[42] = 100\nmask = 00000000000000000000000000000000X0XX\nmem[26] = 1", "208");
  }

  public function processInputs(array $inputs): array {
    $newInputs = [];
    $count = 0;
    $newInput = [
      'mask' => '',
      'ops' => [],
    ];

    foreach ($inputs as $k => $input) {
      if (strpos($input, 'mask') === FALSE) {
        [$mem, $val] = explode(' = ', $input);
        $mem = explode('[', $mem);
        $newInput['ops'][] = [
          'pos' => substr($mem[1],0, strlen($mem[1])-1),
          'val' => $val,
        ];
      }
      else {
        if ($newInput['mask']) {
          $newInputs[$count] = $newInput;
          $count++;
        }

        [$m, $mask] = explode(' = ', $input);

        $newInput = [
          'mask' => $mask,
          'ops' => [],
        ];
      }

      $newInputs[$count] = $newInput;
    }

    return $newInputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $mem = [];

    foreach ($inputs as $input) {
      $mask = $input['mask'];
      foreach ($input['ops'] as $op) {
        $pos = $op['pos'];
        $val = str_pad(decbin((int)$op['val']), 36, '0', STR_PAD_LEFT);

        $val = $this->applyMask($mask, $val);
        $mem[$pos] = bindec($val);
      }
    }

    $answer = array_sum($mem);
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $mem = [];

    foreach ($inputs as $input) {
      $mask = $input['mask'];
      foreach ($input['ops'] as $op) {
        $pos = str_pad(decbin((int)$op['pos']), 36, '0', STR_PAD_LEFT);
        $val = $op['val'];

        $addresses = $this->applyFloatingMask($mask, $pos);

        foreach ($addresses as $address) {
          $mem[bindec($address)] = $val;
        }
      }
    }

    $answer = array_sum($mem);
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  public function applyMask($mask, $value) {
    foreach(str_split($mask) as $k => $m) {
      if ($m !== 'X') {
        $value[$k] = $m;
      }
    }

    return $value;
  }

  public function applyFloatingMask($mask, $value) {
    $addresses = [];
    $address = $value;
    foreach(str_split($mask) as $k => $m) {
      if ($m === 'X') {
        $address[$k] = 'X';
      }
      elseif ($m === '1') {
        $address[$k] = 1;
      }
    }

    do {
      $x = strpos($address, 'X');
      $address[$x] = 'D';

      if (!$addresses) {
        $addresses[] = $address;
      }

      foreach ($addresses as $k => $a) {
        $b = $a;
        $a[$x] = 0;
        $b[$x] = 1;
        $addresses[$k] = $a;
        $addresses[] = $b;
      }

      $xs = strpos($address, 'X');
    } while($xs !== FALSE);

    return $addresses;
  }

}
