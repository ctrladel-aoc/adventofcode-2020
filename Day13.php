<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day13 extends Day {

  protected const DAY = 13;

  public function __construct() {
    $this->addExample(1, 1, "939\n7,13,x,x,59,x,31,19", "295");
    $this->addExample(2, 1, "939\n17,x,13,19", "3417");
    $this->addExample(2, 2, "939\n67,7,59,61", "754018");
    $this->addExample(2, 3, "939\n67,x,7,59,61", "779210");
    $this->addExample(2, 4, "939\n67,7,x,59,61", "1261476");
    $this->addExample(2, 5, "939\n1789,37,47,1889", "1202161486");
  }

  public function processInputs(array $inputs): array {

    $newInputs = [];
    foreach ($inputs as $k => $input) {
      if ($k == 0) {
        $newInputs['arrival'] = $input;
      }
      else {
        $buses = explode(',', $input);
        foreach ($buses as $k => $bus) {
          if($bus !== 'x') {
            $newInputs['buses'][$k] = $bus;
          }
        }
      }
    }

    return $newInputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $arrival = $inputs['arrival'];

    $busId = '';
    $wait = 100000;
    foreach ($inputs['buses'] as $bus) {
      $tWait = (ceil($arrival / $bus) * $bus) - $arrival;

      if ($tWait < $wait) {
        $wait = $tWait;
        $busId = $bus;
      }
    }

    $answer = $busId * $wait;
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $buses = $inputs['buses'];

    $timestamp = reset($buses);
    $period = $timestamp;
    $buses = array_slice($buses, 1, NULL, TRUE);

    foreach ($buses as $key => $bus) {
      while (($timestamp + $key) % $bus !== 0) {
        $timestamp += $period;
      }

      $period = (int) gmp_lcm($period, $bus);
    }

    $answer = $timestamp;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
