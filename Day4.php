<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day4 extends Day {

  protected const DAY = 4;

  public function __construct() {
    $this->addExample(1, 1, "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\nbyr:1937 iyr:2017 cid:147 hgt:183cm\n\niyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\nhcl:#cfa07d byr:1929\n\nhcl:#ae17e1 iyr:2013\neyr:2024\necl:brn pid:760753108 byr:1931\nhgt:179cm\n\nhcl:#cfa07d eyr:2025 pid:166559648\niyr:2011 ecl:brn hgt:59in", "2");
    $this->addExample(2, 1, "eyr:1972 cid:100\nhcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926\n\niyr:2019\nhcl:#602927 eyr:1967 hgt:170cm\necl:grn pid:012533040 byr:1946\n\nhcl:dab227 iyr:2012\necl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277\n\nhgt:59cm ecl:zzz\neyr:2038 hcl:74454a iyr:2023\npid:3556412378 byr:2007", "0");
    $this->addExample(2, 2, "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980\nhcl:#623a2f\n\neyr:2029 ecl:blu cid:129 byr:1989\niyr:2014 pid:896056539 hcl:#a97842 hgt:165cm\n\nhcl:#888785\nhgt:164cm byr:2001 iyr:2015 cid:88\npid:545766238 ecl:hzl\neyr:2022\n\niyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719", "4");
  }

  public function processInputs(array $inputs): array {
    $passports = [];
    $passport = [];
    foreach ($inputs as $input) {
      if ($input !== '') {
        $values = explode(' ', $input);

        foreach ($values as $value) {
          [$k, $v] = explode(':', $value);
          $passport[$k] = $v;
        }
      }
      else {
        ksort($passport);
        $passports[] = $passport;
        $passport = [];
      }
    }

    ksort($passport);
    $passports[] = $passport;

    return $passports;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $requiredKeys = [
      'byr',
      'iyr',
      'eyr',
      'hgt',
      'hcl',
      'ecl',
      'pid',
//      'cid:',
    ];

    $count = 0;
    foreach ($inputs as $input) {
      $valid = TRUE;
      if (array_diff($requiredKeys, array_keys($input))) {
        $valid = FALSE;
      }

      if ($valid) {
        $count++;
      }
    }

    $answer = $count;
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $requiredKeys = [
      'byr',
      'iyr',
      'eyr',
      'hgt',
      'hcl',
      'ecl',
      'pid',
//      'cid:',
    ];

    $count = 0;
    foreach ($inputs as $input) {
      $valid = TRUE;

      if (array_diff($requiredKeys, array_keys($input))) {
        continue;
      }

      foreach ($requiredKeys as $key) {
        $value = $input[$key];

        switch ($key) {
          case 'byr':
            $valid = 1920 <= $value && $value <= 2002;
            break;
          case 'iyr':
            $valid = 2010 <= $value && $value <= 2020;
            break;
          case 'eyr':
            $valid = 2020 <= $value && $value <= 2030;
            break;
          case 'hgt':
            $measured = substr($value, 0, strlen($value) - 2);
            $unit = substr($value, -2);

            if ($unit !== 'cm' && $unit !== 'in') {
              $valid = FALSE;
            }

            if ($unit === 'cm') {
              $valid = 150 <= $measured && $measured <= 193;
            }

            if ($unit === 'in') {
              $valid = 59 <= $measured && $measured <= 76;
            }

            break;
          case 'hcl':
            if (preg_match('/^#[0-9A-F]{6}$/i', $value) === 0) {
              $valid = FALSE;
            }
            break;
          case 'ecl':
            $colors = [
              'amb',
              'blu',
              'brn',
              'gry',
              'grn',
              'hzl',
              'oth',
            ];
            $colors = array_flip($colors);
            $valid = array_key_exists($value, $colors);
            break;
          case 'pid':
            if (preg_match('/^[0-9]{9}$/', $value) === 0) {
              $valid = FALSE;
            }
            break;
        }

        if (!$valid) {
          break;
        }
      }

      if ($valid) {
        $count++;
      }
    }

    $answer = $count;
    echo "\n\nPART 2\n";
    echo "Answer: $answer\n" ;
    return $answer;
  }

}
