<?php

namespace y2020;

use y2020\src\Day;
use y2020\src\ExceptionValue;

require __DIR__ . '/../../autoload.php';

class Day8 extends Day {

  protected const DAY = 8;

  public function __construct() {
    $this->addExample(1, 1, "nop +0\nacc +1\njmp +4\nacc +3\njmp -3\nacc -99\nacc +1\njmp -4\nacc +6", "5");
    $this->addExample(2, 1, "nop +0\nacc +1\njmp +4\nacc +3\njmp -3\nacc -99\nacc +1\njmp -4\nacc +6", "8");
  }

  public function processInputs(array $inputs): array {
    foreach ($inputs as &$input) {
      $input = explode(' ', $input);
    }

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $answer = 'No loop found';
    try {
      $this->runOps($inputs);
    }
    catch (ExceptionValue $e) {
      $answer = $e->getValue();
    }

    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $opsToFlip = [];
    foreach ($inputs as $position => $input) {
      if ($input[0] == 'nop') {
        $opsToFlip[] = [
          'position' => $position,
          'newOp' => 'jmp'
        ];
      }
      if ($input[0] == 'jmp') {
        $opsToFlip[] = [
          'position' => $position,
          'newOp' => 'nop'
        ];
      }
    }

    $answer = 'No operations ran to completion.';
    foreach ($opsToFlip as $flip) {
      try {
        $ops = $inputs;
        $this->changeOp($ops, $flip['position'], $flip['newOp']);

        $finished = $this->runOps($ops);
      }
      catch (ExceptionValue $e) {
        $finished = FALSE;
      }

      if ($finished !== FALSE) {
        $answer = $finished;
        break;
      }
    }

    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  public function runOps($operations) {
    $ran = [];
    $acc = 0;

    for ($position = 0; $position <= count($operations);) {
      if (in_array($position, $ran)) {
        throw new ExceptionValue('Infinite loop detected. Value in the accumulator has been stored.', 0, NULL, $acc);
      }
      else {
        $ran[] = $position;
      }

      if ($position == (count($operations))) {
        break;
      }
      elseif (!isset($operations[$position])) {
        throw new ExceptionValue('Tried to reference a position that does not exist. Position: '  . $position);
      }

      $op = $operations[$position][0];
      $opVal = $operations[$position][1];
      if ($op == 'nop') {
        $position++;
      }
      if ($op == 'acc') {
        $acc += $opVal;
        $position++;
      }
      if ($op == 'jmp') {
        $position += $opVal;
      }
    }

    return $acc;
  }

  public function changeOp(&$operations, $position, $newOp) {
    $operations[$position][0] = $newOp;
  }

}
