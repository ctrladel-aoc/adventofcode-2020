<?php


namespace y2020\src;

use Exception;

class ExceptionValue extends Exception {

  /**
   * The value to log with the exception.
   *
   * @var mixed|null
   */
  protected $value;

  public function __construct($message="", $code=0 , Exception $previous=NULL, $value = NULL) {
    parent::__construct($message, $code, $previous);

    $this->value = $value;
  }

  public function getValue() {
    return $this->value;
  }
}
