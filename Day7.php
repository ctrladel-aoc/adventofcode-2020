<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day7 extends Day {

  protected const DAY = 7;

  public function __construct() {
    $this->addExample(1, 1, "light red bags contain 1 bright white bag, 2 muted yellow bags.\ndark orange bags contain 3 bright white bags, 4 muted yellow bags.\nbright white bags contain 1 shiny gold bag.\nmuted yellow bags contain 2 shiny gold bags, 9 faded blue bags.\nshiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.\ndark olive bags contain 3 faded blue bags, 4 dotted black bags.\nvibrant plum bags contain 5 faded blue bags, 6 dotted black bags.\nfaded blue bags contain no other bags.\ndotted black bags contain no other bags.", "4");
    $this->addExample(2, 1, "light red bags contain 1 bright white bag, 2 muted yellow bags.\ndark orange bags contain 3 bright white bags, 4 muted yellow bags.\nbright white bags contain 1 shiny gold bag.\nmuted yellow bags contain 2 shiny gold bags, 9 faded blue bags.\nshiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.\ndark olive bags contain 3 faded blue bags, 4 dotted black bags.\nvibrant plum bags contain 5 faded blue bags, 6 dotted black bags.\nfaded blue bags contain no other bags.\ndotted black bags contain no other bags.", "32");
    $this->addExample(2, 2, "shiny gold bags contain 2 dark red bags.\ndark red bags contain 2 dark orange bags.\ndark orange bags contain 2 dark yellow bags.\ndark yellow bags contain 2 dark green bags.\ndark green bags contain 2 dark blue bags.\ndark blue bags contain 2 dark violet bags.\ndark violet bags contain no other bags.", "126");
  }

  public function processInputs(array $inputs): array {
    $bags = [];
    foreach ($inputs as $input) {
      $bagPos = strpos($input, ' bags ');
      $bagColor = substr($input, 0, $bagPos);

      $bags[$bagColor] = substr($input, $bagPos + strlen(' bags contain '));
    }

    return $bags;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $bags = [];
    foreach ($inputs as $key => $bag) {
      if (strpos($bag, 'shiny gold') !== FALSE && !in_array($key, $bags)) {
        $bags[] = $key;
      }
    }

    do {
      $startingBags = count($bags);
      foreach ($inputs as $key => $bag) {
        foreach ($bags as $l) {
          if (strpos($bag, $l) !== FALSE && !in_array($key, $bags)) {
            $bags[] = $key;
          }
        }
      }

      if ($startingBags < count($bags)) {
        $newBagsFound = TRUE;
      }
      else {
        $newBagsFound = FALSE;
      }

    } while ($newBagsFound === TRUE);

    $answer = count($bags);
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    foreach ($inputs as $key => $input) {
      $bagString = explode(',', $input);

      $bags = [];
      foreach ($bagString as $bag) {
        $remove = strlen(' bag');
        if (strpos($bag, 'bags')) {
          $remove = strlen(' bags');
        }

        $bag = trim(substr($bag, 0, strlen($bag) - $remove));
        $numPos = strpos($bag, ' ');

        $num = substr($bag, 0, $numPos);
        if (strpos($bag, 'no other') !== FALSE) {
          $num = 0;
        }
        $bagColor = substr($bag, $numPos + 1 );
        $bags[$bagColor] = $num;
      }

      $inputs[$key] = $bags;
    }

    $bags = $this->getBagsContainedIn($inputs, 'shiny gold', 1);

    unset($bags['shiny gold']);

    $answer = array_sum($bags);
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  function getBagsContainedIn($inputs, $color, $needed) {
    $newBags = [];

    $newBags[$color] = $needed;

    foreach ($inputs[$color] as $bagColor => $requiredQty) {
      if ($bagColor === 'other') {
        return [$color => $needed];
      }

      $bagsContained = $this->getBagsContainedIn($inputs, $bagColor, $requiredQty * $needed);

      foreach ($bagsContained as $childBagColor => $childQty) {
        if (!isset($newBags[$childBagColor])) {
          $newBags[$childBagColor] = $childQty;
        }
        else {
          $newBags[$childBagColor] += $childQty;
        }
      }
    }

    return $newBags;
  }

}
