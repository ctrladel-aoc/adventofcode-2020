<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day17 extends Day {

  protected const DAY = 17;

  public function __construct() {
    $this->addExample(1, 1, ".#.\n..#\n###", "112");
    $this->addExample(2, 1, ".#.\n..#\n###", "848");
  }

  public function processInputs(array $inputs): array {

    foreach ($inputs as &$input) {
      $input = str_split($input);
    }

    return [0 => $inputs];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $adj = [
      [-1,-1, -1],
      [-1,-1, 0],
      [-1,-1, 1],
      [-1,0, -1],
      [-1,0, 0],
      [-1,0, 1],
      [-1,1, -1],
      [-1,1, 0],
      [-1,1, 1],
      [0,-1, -1],
      [0,-1, 0],
      [0,-1, 1],
      [0,0, -1],
      [0,0, 1],
      [0,1, -1],
      [0,1, 0],
      [0,1, 1],
      [1,-1, -1],
      [1,-1, 0],
      [1,-1, 1],
      [1,0, -1],
      [1,0, 0],
      [1,0, 1],
      [1,1, -1],
      [1,1, 0],
      [1,1, 1],
    ];

    $zMin = 0;
    $zMax = 0;
    $yMin = 0;
    $yMax = count($inputs[0]);
    $xMin = 0;
    $xMax = count($inputs[0][0]);

    $live = [];
    foreach ($inputs as $z => $grid) {
      foreach ($grid as $y => $row) {
        foreach ($row as $x => $state) {
          if ($state === '#') {
            $live["$z,$y,$x"] = 1;
          }
        }
      }
    }

    for ($r = 0; $r < 6; $r++) {
      $zMin--;
      $zMax++;
      $xMin--;
      $xMax++;
      $yMin--;
      $yMax++;

      $newState = $live;
      for ($z = $zMin; $z <= $zMax; $z++) {
        for ($y = $yMin; $y <= $yMax; $y++) {
          for ($x = $xMin; $x <= $xMax; $x++) {
            if (isset($live["$z,$y,$x"])) {
              $state = 1;
            }
            else {
              $state = 0;
            }

            $liveNeighbors = 0;
            foreach ($adj as $pos) {
              $az = $pos[0] + $z;
              $ay = $pos[1] + $y;
              $ax = $pos[2] + $x;
              if (isset($live["$az,$ay,$ax"])) {
                $liveNeighbors++;
              }
            }

            if ($state) {
              if ($liveNeighbors == 3 || $liveNeighbors == 2) {
                $newState["$z,$y,$x"] = 1;
              }
              else {
                unset($newState["$z,$y,$x"]);
              }
            }
            else {
              if ($liveNeighbors == 3) {
                $newState["$z,$y,$x"] = 1;
              }
              else {
                unset($newState["$z,$y,$x"]);
              }
            }
          }
        }
      }

      $live = $newState;
    }

    $answer = count($live);

    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $zMin = 0;
    $zMax = 0;
    $wMin = 0;
    $wMax = 0;
    $yMin = 0;
    $yMax = count($inputs[0]);
    $xMin = 0;
    $xMax = count($inputs[0][0]);

    $live = [];
    foreach ($inputs as $z => $grid) {
      foreach ($grid as $y => $row) {
        foreach ($row as $x => $state) {
          if ($state === '#') {
            $live["0,$z,$y,$x"] = 1;
          }
        }
      }
    }

    for ($r = 0; $r < 6; $r++) {
      $zMin--;
      $zMax++;
      $xMin--;
      $xMax++;
      $yMin--;
      $yMax++;
      $wMin--;
      $wMax++;

      $newState = $live;
      for ($w = $wMin; $w <= $wMax; $w++) {
        for ($z = $zMin; $z <= $zMax; $z++) {
          for ($y = $yMin; $y <= $yMax; $y++) {
            for ($x = $xMin; $x <= $xMax; $x++) {
              if (isset($live["$w,$z,$y,$x"])) {
                $state = 1;
              }
              else {
                $state = 0;
              }

              $liveNeighbors = 0;
              $aYMin = $y - 1;
              $aYMax = $y + 1;
              $aXMin = $x - 1;
              $aXMax = $x + 1;
              $aZMin = $z - 1;
              $aZMax = $z + 1;
              $aWMin = $w - 1;
              $aWMax = $w + 1;
              for ($aw = $aWMin; $aw <= $aWMax; $aw++) {
                for ($az = $aZMin; $az <= $aZMax; $az++) {
                  for ($ay = $aYMin; $ay <= $aYMax; $ay++) {
                    for ($ax = $aXMin; $ax <= $aXMax; $ax++) {
                      if ("$w,$z,$y,$x" !== "$aw,$az,$ay,$ax") {
                        if (isset($live["$aw,$az,$ay,$ax"])) {
                          $liveNeighbors++;
                        }
                      }
                    }
                  }
                }
              }

              if ($state) {
                if ($liveNeighbors == 3 || $liveNeighbors == 2) {
                  $newState["$w,$z,$y,$x"] = 1;
                }
                else {
                  unset($newState["$w,$z,$y,$x"]);
                }
              }
              else {
                if ($liveNeighbors == 3) {
                  $newState["$w,$z,$y,$x"] = 1;
                }
                else {
                  unset($newState["$w,$z,$y,$x"]);
                }
              }
            }
          }
        }
      }

      $live = $newState;
    }

    $answer = count($live);
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
