<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day2 extends Day {

  protected const DAY = 2;

  public function __construct() {
    $this->addExample(1, 1, "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc", "2");
    $this->addExample(2, 1, "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc", "1");
  }

  public function processInputs(array $inputs): array {
    foreach ($inputs as &$input) {
      $values = explode(' ', $input);
      $input = [];

      $nums = explode('-',$values[0]);
      $input['num1'] = $nums[0];
      $input['num2'] = $nums[1];

      $input['letter'] = $values[1][0];
      $input['password'] = $values[2];
    }

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $matches = [];
    foreach ($inputs as $item) {
      $count = substr_count($item['password'], $item['letter']);
      $min = $item['num1'];
      $max = $item['num2'];

      if ( $count >= $min && $count <= $max ) {
        $matches[] = $item;
      }
    }

    $answer = count($matches);
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $matches = [];
    foreach ($inputs as $item) {
      $pos1 = $item['num1'];
      $pos2 = $item['num2'];

      $pos1Match = $item['password'][$pos1-1] === $item['letter'];
      $pos2Match = $item['password'][$pos2-1] === $item['letter'];

      if ($pos1Match xor $pos2Match) {
        $matches[] = $item;
      }
    }

    $answer = count($matches);
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}

