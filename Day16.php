<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day16 extends Day {

  protected const DAY = 16;

  public function __construct() {
    $this->addExample(1, 1, "class: 1-3 or 5-7\nrow: 6-11 or 33-44\nseat: 13-40 or 45-50\n\nyour ticket:\n7,1,14\n\nnearby tickets:\n7,3,47\n40,4,50\n55,2,20\n38,6,12", "71");
    $this->addExample(2, 1, "class: 0-1 or 4-19\nrow: 0-5 or 8-19\nseat: 0-13 or 16-19\n\nyour ticket:\n11,12,13\n\nnearby tickets:\n3,9,18\n15,1,5\n5,14,9", "1");
  }

  public function processInputs(array $inputs): array {
    $empty = 0;

    $newInputs = [
      'notes' => [],
      'ticket' => [],
      'nearby' => [],
    ];

    foreach ($inputs as $input) {
      if (!$input) {
        $empty++;
        continue;
      }
      if ($empty == 0) {
        [$name, $ranges] = explode(': ', $input);

        $ranges = explode(' or ', $ranges);

        $validValues = [];
        foreach ($ranges as $range) {
          [$min, $max] = explode('-', $range);

          for ($i = $min; $i <= $max; $i++) {
            $validValues[] = (int) $i;
          }
        }

        $newInputs['notes'][$name] = $validValues;
      }
      if ($empty == 1 && strpos($input, 'your ticket:') === FALSE) {
        $newInputs['ticket'][] = explode(',', $input);
      }
      if ($empty == 2 && strpos($input, 'nearby tickets:') === FALSE) {
        $newInputs['nearby'][] = explode(',', $input);
      }
    }

    return $newInputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $validNearby = [];
    $invalidNearby = [];
    $invalidNumbers = [];
    foreach ($inputs['nearby'] as $ticket) {
      $valid = [];
      foreach ($ticket as $value) {
        $validField = [];
        foreach ($inputs['notes'] as $name => $validValues) {
          if(in_array($value, $validValues)) {
            $validField[$name] = $name;
          }
        }

        if (!$validField) {
          $valid = [];
          $invalidNumbers[] = $value;
          break;
        }
        else {
          $valid = array_merge($valid, $validField);
        }
      }

      if ($valid && count($valid) >= count($ticket)) {
        $validNearby[] = $ticket;
      }
      else {
        $invalidNearby[] = $ticket;
      }
    }


    $answer = array_sum($invalidNumbers);
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $validNearby = [];
    $invalidNearby = [];
    $invalidNumbers = [];
    foreach ($inputs['nearby'] as $ticket) {
      $valid = [];
      foreach ($ticket as $value) {
        $validField = [];
        foreach ($inputs['notes'] as $name => $validValues) {
          if(in_array($value, $validValues)) {
            $validField[$name] = $name;
          }
        }

        if (!$validField) {
          $valid = [];
          $invalidNumbers[] = $value;
          break;
        }
        else {
          $valid = array_merge($valid, $validField);
        }
      }

      if ($valid && count($valid) >= count($ticket)) {
        $validNearby[] = $ticket;
      }
      else {
        $invalidNearby[] = $ticket;
      }
    }

    $possibleMatches = array_flip(array_keys($inputs['notes']));
    foreach ($inputs['notes'] as $field => $validFieldValues) {
      for ($i = 0; $i < count($validNearby[0]); $i++) {
        $invalid = FALSE;
        foreach ($validNearby as $value) {
          if (!in_array($value[$i], $validFieldValues)) {
            $invalid = TRUE;
            break;
          }
        }

        if (!$invalid) {
          if(!is_array($possibleMatches[$field])) {
            $possibleMatches[$field] = [];
          }
          $possibleMatches[$field][] = $i;
        }
      }
    }

    $matches = [];
    do {
      $more = FALSE;

      foreach ($possibleMatches as $k => $possibleMatch) {
        if (count($possibleMatch) == 1) {
          $m = reset($possibleMatch);
          $matches[$k] = $m;
          foreach ($possibleMatches as &$p) {
            unset($p[array_search($m, $p)]);
          }

          unset($possibleMatches[$k]);
        }
      }

    } while($possibleMatches);

    $ticket = $inputs['ticket'];

    $answer = 1;
    foreach ($matches as $field => $pos) {
      if (strpos($field, 'departure') !== FALSE) {
        $answer = $answer * $ticket[0][$pos];
      }
    }

    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
