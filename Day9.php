<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day9 extends Day {

  protected const DAY = 9;

  public function __construct() {
    $this->addExample(1, 1, "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n24\n25\n26", "All numbers are valid");
    $this->addExample(1, 2, "2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n24\n25\n26\n49", "All numbers are valid");
    $this->addExample(1, 3, "3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n24\n25\n26\n49\n100", "100");
    $this->addExample(1, 4, "3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n3\n25\n26\n49\n50", "50");
    $this->addExample(1, 5, "35\n20\n15\n25\n47\n40\n62\n55\n65\n95\n102\n117\n150\n182\n127\n219\n299\n277\n309\n576", "127", [5]);
    $this->addExample(2, 1, "35\n20\n15\n25\n47\n40\n62\n55\n65\n95\n102\n117\n150\n182\n127\n219\n299\n277\n309\n576", "62", [5]);
  }

  public function processInputs(array $inputs): array {
    return $inputs;
  }

  public function getAnswerPart1($preamble = 25) {
    $inputs = $this->getInputs();

    $numbers = array_splice($inputs, 0, $preamble);

    $answer = 'All numbers are valid';
    foreach ($inputs as $k => $input) {
      $sumsToInput = $this->findTargetSumWithNValues($input, 2, $numbers);

      if (!empty($sumsToInput)) {
        $numbers[] = $input;
        array_shift($numbers);
      }
      else {
        $answer = $input;
        break;
      }

    }

    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2($preamble = 25) {
    $inputs = $this->getInputs();

    $target = $this->getAnswerPart1($preamble);
    $found = FALSE;
    $contiguous = 3;
    $numbers = [];
    do {
      $ins = $inputs;
      for ($i=0; $i< count($inputs); $i++) {
        $chunks = array_chunk($ins, $contiguous);
        foreach ($chunks as $chunk) {
          if (count($chunk) == $contiguous && array_sum($chunk) == $target) {
            $numbers = $chunk;
            $found = TRUE;
            break;
          }
        }

        if ($found) {
          break;
        }

        $first = array_shift($ins);
        $ins[] = $first;
      }

      if (!$found) {
        $contiguous++;
      }
    }while($found == FALSE);

    $number1 = max($numbers);
    $number2 = min($numbers);

    $answer = $number1 + $number2;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  function findTargetSumWithNValues($target, $numberOfValues, $values) {
    $answers = [];
    foreach ($values as $key => $value) {
      if ($numberOfValues > 2) {
        $available = $values;
        unset($available[$key]);
        $answers = $this->findTargetSumWithNValues($target - $value, $numberOfValues-1, $available);
        if ($answers) {
          $answers[] = $value;
          break;
        }
      }
      else {
        foreach ($values as $key2 => $value2) {
          if (($value + $value2) == $target && $key !== $key2) {
            return [$value, $value2];
          }
        }
      }
    }

    return $answers;
  }

}
