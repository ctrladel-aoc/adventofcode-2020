<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day19 extends Day {

  protected const DAY = 19;

  public function __construct() {
    $this->addExample(1, 1, "0: 4 1 5\n1: 2 3 | 3 2\n2: 4 4 | 5 5\n3: 4 5 | 5 4\n4: a\n5: b\n\nababbb\nbababa\nabbbab\naaabbb\naaaabbb", "2");
    $this->addExample(2, 1, "", "");
  }

  public function processInputs(array $inputs): array {
    $rules = [];
    $messages = [];
    $doMessages = FALSE;
    foreach ($inputs as $input) {
      if (empty($input)) {
        $doMessages = TRUE;
        continue;
      }

      if (!$doMessages) {
        [$num, $rule] = explode(': ', $input);
        $rules[$num] = $rule;
      }
      else {
        $messages[] = $input;
      }
    }

    return [$rules, $messages];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $rules = $inputs[0];
    $messages = $inputs[1];

    $rule = explode(' ', $rules[0]);

    do {
      foreach ($rule as &$part) {
        if (!is_array($part)) {
          $part = [$part];
        }
        foreach ($part as &$r) {
          if ($r == 'a' || $r == 'b' || $r == '|') {
            continue;
          }
          else {
            $r = explode(' | ', $rules[$r]);
          }
        }
      }

      $rule = implode(' ', $rule);
      $a = 5;
    } while(preg_match("/[\d]/i", $rule));


    $answer = '';
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $answer = '';
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
