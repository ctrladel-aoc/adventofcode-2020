<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day12 extends Day {

  protected const DAY = 12;

  public function __construct() {
    $this->addExample(1, 1, "F10\nN3\nF7\nR90\nF11", "25");
    $this->addExample(2, 1, "F10\nN3\nF7\nR90\nF11", "286");
  }

  public function processInputs(array $inputs): array {

    foreach ($inputs as &$input) {
      $input = [
        $input[0],
        substr($input, 1),
      ];
    }

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $x = 0;
    $y = 0;

    $d = 90;
    foreach ($inputs as $input) {
      switch ($input[0]) {
        case 'N':
          $x -= $input[1];
          break;
        case 'S':
          $x += $input[1];
          break;
        case 'E':
          $y += $input[1];
          break;
        case 'W':
          $y -= $input[1];
          break;
        case 'L':
          $rotate = $input[1] % 360;
          $d -= $rotate;

          if ($d < 0) {
            $d = 360 + $d;
          }

          break;
        case 'R':
          $rotate = $input[1] % 360;
          $d += $rotate;

          if ($d >= 360) {
            $d = $d - 360;
          }
          break;
        case 'F':
          if ($d == 0) {
            $x -= $input[1];
          }
          elseif ($d == 90) {
            $y += $input[1];
          }
          elseif ($d == 180) {
            $x += $input[1];
          }
          elseif ($d == 270) {
            $y -= $input[1];
          }
          break;
      }
    }

    $answer = abs($x) + abs($y);
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $x = 0;
    $y = 0;

    $wayX = 10;
    $wayY = -1;
    foreach ($inputs as $input) {
      switch ($input[0]) {
        case 'N':
          $wayY -= $input[1];
          break;
        case 'S':
          $wayY += $input[1];
          break;
        case 'E':
          $wayX += $input[1];
          break;
        case 'W':
          $wayX -= $input[1];
          break;
        case 'L':
          $d = $input[1] % 360;

          if ($d < 0) {
            $d = 360 + $d;
          }

          if ($d == 90) {
            $tX = $wayX;
            $tY = $wayY;

            $wayY = -$tX;
            $wayX = $tY;
          }
          elseif ($d == 180) {
            $tX = $wayX;
            $tY = $wayY;

            $wayY = -$tY;
            $wayX = -$tX;
          }
          elseif ($d == 270) {
            $tX = $wayX;
            $tY = $wayY;

            $wayY = $tX;
            $wayX = -$tY;
          }

          break;
        case 'R':
          $d = $input[1] % 360;

          if ($d == 90) {
            $tX = $wayX;
            $tY = $wayY;

            $wayY = $tX;
            $wayX = -$tY;
          }
          elseif ($d == 180) {
            $tX = $wayX;
            $tY = $wayY;

            $wayY = -$tY;
            $wayX = -$tX;
          }
          elseif ($d == 270) {
            $tX = $wayX;
            $tY = $wayY;

            $wayY = -$tX;
            $wayX = $tY;
          }
          break;
        case 'F':
          for ($i = 0; $i < $input[1]; $i++) {
            $x += $wayX;
            $y += $wayY;
          }
          break;
      }
    }

    $answer = abs($x) + abs($y);
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
